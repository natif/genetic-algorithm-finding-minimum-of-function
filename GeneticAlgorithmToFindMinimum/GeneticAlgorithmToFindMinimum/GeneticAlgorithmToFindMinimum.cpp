// GeneticAlgorithmToFindMinimum.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include<cmath>
#include<time.h>
#include<algorithm>

using namespace std;

#define POPSIZE 100
#define GENSIZE 6

struct Points {
	float x;
	float z;
	float v;
	float w;
	float fitness;
};

int functionY(Points point);
void createPopulation(Points points[POPSIZE]);
void crossing(Points &father, Points &mother);
void mutation(Points &points, float mutationStep);
int parseBitValue(short int bit[GENSIZE]);

#define POPSIZE 100

bool operator<(const Points& lhs, const Points& rhs) {
  return lhs.fitness < rhs.fitness;
}

int _tmain(int argc, _TCHAR* argv[])
{
	Points currentPopulation[POPSIZE];
	clock_t c1,c2;
	c1 = clock();
	createPopulation(currentPopulation);

	int generation = 0;
	float acceptableMistake = 0.01;
	float mutationMeasure = 0.05f;
	float crossingMeasure = 0.7f;
	float mutationStep = 0.1f;
	bool found = false;

	while (!found) {
		sort(currentPopulation,currentPopulation+POPSIZE);

		for(int i=0; i < POPSIZE; i++) {
			if(currentPopulation[i].fitness < acceptableMistake) {
				found = true;
			}

			if (found == true) {
				cout<<"\nFactors X: "<<currentPopulation[i].x<<" Z: "<<currentPopulation[i].z<< " V: "<< currentPopulation[i].v<< " W: "<< currentPopulation[i].w;
				cout<<"\n\nFITNESS "<<currentPopulation[i].fitness;
				cout<<"\n\nSolution found in "<<generation<<" generation ";
				break;
			}
		}

		for(int i=0;i<(POPSIZE*crossingMeasure);i++) {
			crossing(currentPopulation[i],currentPopulation[i+2]);
		}

		for(int i=0;i<(POPSIZE*mutationMeasure);i++) {
			mutation(currentPopulation[i], mutationStep);
			mutation(currentPopulation[i+2], mutationStep);
		}

		for(int i = 0; i < POPSIZE; i++) {
			 currentPopulation[i].fitness = currentPopulation[i].x*currentPopulation[i].x + currentPopulation[i].z*currentPopulation[i].z + currentPopulation[i].v*currentPopulation[i].v + currentPopulation[i].w*currentPopulation[i].w;
		}
		
		 generation++;
	}     

	c2 = clock();
    cout<<"and "<<((float)c2-(float)c1)/CLOCKS_PER_SEC<<" seconds."<<endl<<endl;

	system("pause");
	return 0;
}


void createPopulation(Points points[]) 
{

	int max = 3;
	int min = -2;
	for(int i = 0; i < POPSIZE; i++){
		points[i].x = ((max - min) * ( (float)rand() / (float)RAND_MAX ) + min);
		points[i].z = ((max - min) * ( (float)rand() / (float)RAND_MAX ) + min);
		points[i].v = ((max - min) * ( (float)rand() / (float)RAND_MAX ) + min);
		points[i].w = ((max - min) * ( (float)rand() / (float)RAND_MAX ) + min);
		points[i].fitness = points[i].x*points[i].x + points[i].z*points[i].z + points[i].v*points[i].v + points[i].w*points[i].w;    
	}
              
}    

void mutation(Points &points, float mutationStep) {
	int choose = rand() % 8;
	switch(choose){
		case 0:
			points.x += mutationStep;
			break;
		case 1:
			points.x -= mutationStep;
			break;
		case 2:
			points.z += mutationStep;
			break;
		case 3:
			points.z -= mutationStep;
			break;
		case 4:
			points.v += mutationStep;
			break;
		case 5:
			points.v -= mutationStep;
			break;
		case 6:
			points.w += mutationStep;
			break;
		case 7:
			points.w -= mutationStep;
			break;
	}

} 

void crossing(Points &father, Points &mother) {
  int choose = rand() % 4;
	float temporaryFactor = 0.0f;
	switch(choose){
		case 0:
			temporaryFactor = father.x;
			father.x = mother.x;
			mother.x = temporaryFactor;
			break;
		case 1:
			temporaryFactor = father.z;
			father.z = mother.z;
			mother.z = temporaryFactor;
			break;
		case 2:
			temporaryFactor = father.v;
			father.v = mother.v;
			mother.v = temporaryFactor;
			break;
		case 3:
			temporaryFactor = father.w;
			father.w = mother.w;
			mother.w = temporaryFactor;
			break;
	}
} 


